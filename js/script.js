import Card from "./class.js";

const addingForm = document.querySelector('.adding'),
      addBtn = document.querySelector('.add__btn'),
      closeBtn = addingForm.querySelector('.close');

      
renderData();


async function getData(url) {
    const response = await fetch(url);
    return await response.json();
}

async function renderData() {
    try {
        const [users, posts] = await Promise.all([
            getData('https://ajax.test-danit.com/api/json/users'),
            getData('https://ajax.test-danit.com/api/json/posts')
        ]);
        
        posts.forEach(({id, userId, title, body}) => {
            const userPosts = users.filter(({id}) => id == userId);
            const [{name, email}] = userPosts;
            const bodyText = body[0].toUpperCase() + body.slice(1);
    
            new Card(id, name, email, title, bodyText).render('#root');
        });
    
    } catch(e) {
        console.log(e);
    } finally {
        addBtn.style.display = 'block';
        document.querySelector('.loader').style.display = 'none';
    }
}

addBtn.addEventListener('click', () => addingForm.style.display = 'flex');
closeBtn.addEventListener('click', () => addingForm.style.display = 'none');

addingForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const target = e.target;

    const body = {
        userId: 1,
    };

    target.querySelectorAll('input').forEach(input => {
        body[input.name] = input.value;
    });

    try {
        fetch('https://ajax.test-danit.com/api/json/posts', {
            method: 'POST', 
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        })
        .then(response => response.json())
        .then(result => {
            console.log(result);
        
            document.querySelector('#root').innerHTML = '';
            renderData();
        });
    } catch(error) {
        console.log(error);
    } finally {
        target.reset();
        addingForm.style.display = 'none';
    }
});