class Card {
    constructor(id, name, email, title, text) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.title = title;
        this.text = text;
        this.li = document.createElement('li');
        this.btnDelete = document.createElement('button');
    }

    createCard() {
        this.li.className = 'vocabulary__item';
        this.li.innerHTML = `
            <div class="vocabulary__body">
                <h3 class="user">
                    <span class="name">${this.name}</span>
                    <span class="translation">${this.email}</span>
                </h3>
                
                <h2 class="title">${this.title}</h2>                
                <p class="text">${this.text}</p>                
            </div>
        `;
        this.btnDelete.className = "delete__btn";
        this.btnDelete.innerText = "Delete";

        this.li.append(this.btnDelete);
        this.onDelete();
    }

    onDelete() {
        this.btnDelete.addEventListener('click', () => {
            try {
                fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
                    method: 'DELETE'
                })
                .then(response => {
                    if (response.status == 200) {
                        this.btnDelete.parentElement.remove();
                    }
                });
            } catch(e) {
                console.log(e);
            }
        });
    }

    render(selector) {
        this.createCard();
        document.querySelector(selector).append(this.li);
    }
}

export default Card;